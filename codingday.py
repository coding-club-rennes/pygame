#!/usr/bin/python3

import pygame # permet d'utiliser pygame

############### Gestion des sprites ##############

title = "Zelda Laby" # on nomme la fenetre de notre jeu
image_icone = "images/link_right.gif" #icone de la fenetre du jeu
image_intro = "images/intro.jpg" #premiere image
image_back = "images/back.jpg" #image de fond de jeu
image_wall = "images/wall.gif" #sprite du mur
image_start = "images/thumb_Hole.gif" #sprite du point de depart
image_triforce = "images/triforce.gif" #sprite de triforce / point de fin

nb_sprite = 15
sprite_size = 30
window_size = nb_sprite * sprite_size #resolution

############### Generation de la map #############

class Map: #initialisation de la map
	def __init__(self, file):
		self.file = file
		self.structure = 0
	
	def create(self): #creation de la map en ouvrant un fichier pour y recuperer ses informations
		with open(self.file, "r") as file:
			structure_map = []
			for line in file:
				map_line = []
				for sprite in line:
						map_line.append(sprite)
				structure_map.append(map_line)
			self.structure = structure_map
	
	def show(self, window): #affichage de la map et des objets pouvant s'y trouver
		wall = pygame.image.load(image_wall).convert()
		start = pygame.image.load(image_start).convert()
		triforce = pygame.image.load(image_triforce).convert_alpha()
		
		nb_line = 0
		for line in self.structure:
			nb_pos = 0
			for sprite in line: #calcul des differentes "cases" de la map 
				x = nb_pos * sprite_size
				y = nb_line * sprite_size
				if sprite == 'W':	# W = mur	   
					window.blit(wall, (x,y))
				elif sprite == 'L':	# L = position de depart du personnage	   
					window.blit(start, (x,y))
				elif sprite == 'T':	# T = position de la triforce	   
					window.blit(triforce, (x,y))
				nb_pos = nb_pos + 1
			nb_line = nb_line + 1


			
class Perso:
	def __init__(self, right, left, up, down, map): #creation du personnage et chargement de ses sprites
		self.right = pygame.image.load(right).convert_alpha()
		self.left = pygame.image.load(left).convert_alpha()
		self.up = pygame.image.load(up).convert_alpha()
		self.down = pygame.image.load(down).convert_alpha()

		self.case_x = 0 #on positionne le personnage créé
		self.case_y = 0
		self.x = 0
		self.y = 0

		self.direction = self.down #on cree l'attribu de deplacement du personnage
		self.map = map #permet au personnage de se "reperer"

                
	def move(self, direction): # gestion des deplacements du personnage
		if direction == 'right':
			if self.case_x < (nb_sprite - 1):
				if self.map.structure[self.case_y][self.case_x+1] != 'W': # gestion des murs non franchissables
					self.case_x += 1
					self.x = self.case_x * sprite_size
			self.direction = self.right


############### Boucle de jeu ####################

pygame.init() # on initialise pygame

window = pygame.display.set_mode((window_size, window_size)) # on cree la fenetre
icone = pygame.image.load(image_icone) 

pygame.display.set_icon(icone)
pygame.display.set_caption(title)

loop = 1
while loop: # boucle principale du programme
	intro = pygame.image.load(image_intro).convert()
	window.blit(intro, (0,0))

	pygame.display.flip()

	loop_intro = 1
	loop_game = 1

	while loop_intro: # boucle de l'image d'intro
		for event in pygame.event.get():
			if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE: # quitter le programme
				loop_intro = 0
				loop_game = 0
				loop = 0
				load_map = 0
			elif event.type == pygame.KEYDOWN: # lancer le jeu en appuyant sur "entrée"				
                                if event.key == pygame.K_RETURN:
                                        loop_intro = 0
                                        load_map = 'LinkToThePast' # chargement de la map LinkToThePast



	if load_map != 0: # affichage des elements du jeu si tout fonctionne jusque la
		back = pygame.image.load(image_back).convert()
		map = Map(load_map)
		map.create()
		map.show(window)
		link = Perso("images/link_right.gif", "images/link_left.gif", 
		           "images/link_up.gif", "images/link_down.gif", map)


	while loop_game: # boucle du jeu
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				loop_game = 0
				loop = 0
			elif event.type == pygame.KEYDOWN: # gestion des évènements clavier
				if event.key == pygame.K_ESCAPE:
					loop_game = 0
				elif event.key == pygame.K_RIGHT:
					link.move('right')
                                

		window.blit(back, (0,0)) # mise a jour de l'affichage a chaque action
		map.show(window)
		window.blit(link.direction, (link.x, link.y))
		pygame.display.flip()
